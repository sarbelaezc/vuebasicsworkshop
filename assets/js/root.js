var app = new Vue({
    el: '#listApp',
    data() {
        return {
            objetos: [{ nombre: 'objeto1', precio: 200, cantidad: 0 }, { nombre: 'objeto2', precio: 500, cantidad: 0 }, { nombre: 'objeto3', precio: 300, cantidad: 0 }],
            objetosEnCarrito: [],
            // totalObjetos: 0
        }
    },
    computed: {
        precioTotal() {
            total = 0;
            if (this.objetosEnCarrito.length > 0) {
                this.objetosEnCarrito.forEach(element => {
                    total += element.precio;
                });
            }
            return total;
        },
        totalObjetosEnCarrito() {
            totalObjetos = 0
            this.objetos.forEach(objeto => {
                totalObjetos += objeto.cantidad
            })
            return totalObjetos
        }
    },
    methods: {
        comprarObjeto(nombre) {
            this.objetos.forEach(objeto => {
                if (nombre == objeto.nombre) {
                    objeto.cantidad += 1
                }
            });
        },
        eliminarObjeto(nombre) {
            this.objetos.forEach(objeto => {
                if (nombre == objeto.nombre) {
                    objeto.cantidad -= 1
                }
            });
        }
    }

})